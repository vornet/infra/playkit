# mkit - Simple Makefile target helper
# See LICENSE file for copyright and license details.

export MKIT_DIR

all: build

_mkit_data:
	@"$(MKIT_DIR)"/make _mkit_data

build:
	@"$(MKIT_DIR)"/make build

clean:
	@"$(MKIT_DIR)"/make clean

debstuff: dist
	@"$(MKIT_DIR)"/make debstuff

dist: clean
	@"$(MKIT_DIR)"/make dist

rpmstuff: dist
	@"$(MKIT_DIR)"/make rpmstuff

install: all
	@"$(MKIT_DIR)"/make install

release:
	@"$(MKIT_DIR)"/make release

release_x:
	@"$(MKIT_DIR)"/make release_x

release_y:
	@"$(MKIT_DIR)"/make release_y

release_z:
	@"$(MKIT_DIR)"/make release_z

uninstall:
	@"$(MKIT_DIR)"/make uninstall

vbump:
	@"$(MKIT_DIR)"/make vbump

vbump_x:
	@"$(MKIT_DIR)"/make vbump_x

vbump_y:
	@"$(MKIT_DIR)"/make vbump_y

vbump_z:
	@"$(MKIT_DIR)"/make vbump_z

.PHONY: all _mkit_data clean dist rpmstuff install uninstall release release_x release_y release_z vbump vbump_x vbump_y vbump_z
